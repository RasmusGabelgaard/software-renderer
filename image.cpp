#include "image.hpp"
#include <fstream>

unsigned Image::width() const
{
    return m_width;
}

unsigned Image::height() const
{
    return m_height;
}

unsigned Image::getIndex(unsigned column, unsigned row) const
{
    return 3*(row*width()+column);
}

Colour Image::getPixel(unsigned x, unsigned y) const
{
    Colour c;
    c.r = m_data[getIndex(x,y)+0];
    c.g = m_data[getIndex(x,y)+1];
    c.b = m_data[getIndex(x,y)+2];
    return c;
}

void Image::setPixel(unsigned x, unsigned y, Colour col)
{
    unsigned position = getIndex(x,y);
    m_data[position+0] = col.r;
    m_data[position+1] = col.g;
    m_data[position+2] = col.b;
}

void Image::resize(unsigned resizedWidth, unsigned resizedHeight)
{
    m_width = resizedWidth;
    m_height = resizedHeight;
    m_data.clear();
    m_data.resize(3*resizedWidth*resizedHeight);
}

void writeImage(const Image& img, std::string filename)
{
    std::ofstream out;
    out.open(filename);
    out << "P6\n" << img.width() << " " << img.height() << "\n255\n";
    out.close();
    out.open(filename, std::ios::binary | std::ios::app);
    out.write((const char*)img.m_data.data(), img.m_data.size());
}
